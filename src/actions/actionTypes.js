//export const FETCH_DATA = "FETCH_DATA";
export const REQUEST_DATA = "REQUEST_DATA";
export const RECEIVE_DATA = "RECEIVE_DATA";
export const ERROR_DATA = "ERROR_DATA";
export const ADD_MESSAGE = "ADD_MESSAGE";
export const EDIT_MESSAGE = "EDIT_MESSAGE";
export const LIKE_MESSAGE = "LIKE_MESSAGE";
