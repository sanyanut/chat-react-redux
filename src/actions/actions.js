//https://edikdolynskyi.github.io/react_sources/messages.json

import {
  REQUEST_DATA,
  RECEIVE_DATA,
  ERROR_DATA,
  ADD_MESSAGE,
  EDIT_MESSAGE,
  LIKE_MESSAGE,
} from "../actions/actionTypes";

function requestData() {
  return {
    type: REQUEST_DATA,
  };
}

function receiveData(data) {
  return {
    type: RECEIVE_DATA,
    data: data,
    receivedAt: Date.now(),
  };
}

function errorData() {
  return {
    type: ERROR_DATA,
  };
}

export function addMessage(text) {
  console.log(text);
  return {
    type: ADD_MESSAGE,
    payload: {
      text,
      id: new Date().getTime(),
      createdAt: Date.now(),
      updatedAt: "",
    },
  };
}

export function editMessage(data, id) {
  return {
    type: EDIT_MESSAGE,
    payload: {
      data: data,
      id: id,
    },
  };
}

export function likeMessage(id) {
  return {
    type: LIKE_MESSAGE,
    payload: {
      id,
    },
  };
}

export function fetchData() {
  return function (dispatch) {
    dispatch(requestData());
    return fetch("https://edikdolynskyi.github.io/react_sources/messages.json")
      .then((response) => response.json())
      .then((data) => dispatch(receiveData(data)))
      .catch((error) => {
        dispatch(errorData());
      });
  };
}
