import React from "react";
import { Message } from "../containers/Message";
import { AddMessage } from "../containers/AddMessage";
import store from "../store";

export default class ChatBody extends React.Component {
  render() {
    const data = this.props.data;

    return (
      <div className="chat-body">
        {console.log(data)}
        <ul className="chat-body-messages">
          {data.map((item, index) => {
            return <Message item={item} key={index} />;
          })}
        </ul>
        <AddMessage />
      </div>
    );
  }
}
