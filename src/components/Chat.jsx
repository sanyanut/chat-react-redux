//import "../styles/common.css";
import React from "react";
import { Spinner } from "../containers/Spinner";
import ChatWrapper from "./ChatWrapper";
import { fetchData } from "../actions/actions";
import { connect } from "react-redux";

class Chat extends React.Component {
  componentDidMount() {
    this.props.fetchData();
  }

  render() {
    const { data, isFetching, error } = this.props;
    console.log(data);
    if (isFetching) {
      return <Spinner />;
    } else if (error) {
      return <ChatWrapper error />;
    } else {
      return <ChatWrapper data={data} />;
    }
  }
}

function mapStateToProps(state) {
  return {
    data: state.items,
    isFetching: state.isFetching,
    error: state.error,
  };
}

export default connect(mapStateToProps, { fetchData })(Chat);
