import React from "react";
import { Header } from "../containers/Header";
import ChatBody from "./ChatBody";

const Error = () => {
  return (
    <div className="chat-wrapper">
      <h1>Error! Failed to load.</h1>
    </div>
  );
};

export default class ChatWrapper extends React.Component {
  render() {
    const { data, error } = this.props;

    if (error) {
      return <Error />;
    } else {
      return (
        <div className="chat-wrapper">
          <Header data={data} />
          <ChatBody data={data} />
        </div>
      );
    }
  }
}
