import { MessagesCounter as Messages } from "../helpers/HelperFunctions";
import { UsersCounter as Users } from "../helpers/HelperFunctions";
import { LastMessage } from "../helpers/HelperFunctions";

export const Header = (props) => {
  const data = props.data;

  const messages = Messages(data);
  const users = Users(data);
  const lastMessage = LastMessage(data);

  return (
    <div className="chat-header">
      <div className="chat-header-wrapper">
        <div className="chat-header__logo">My React chat</div>
        <div className="chat-header__counter">
          <span>{users} participants </span>
          <span>{messages} messages</span>
        </div>
        <div className="chat-header__message">
          <span>last message at {lastMessage}</span>
        </div>
      </div>
    </div>
  );
};
