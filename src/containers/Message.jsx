import React, { useState } from "react";
import {
  ConvertDate,
  ActiveLikeIcon,
  LikeIcon,
} from "../helpers/HelperFunctions";

export const Message = (props) => {
  const [isLike, setLike] = useState(false);
  const item = props.item;
  const userInput = props.userInput;

  const handleLikeActive = () => {
    setLike(true);
  };

  const handleLike = () => {
    setLike(false);
  };

  return (
    <li className="chat-body-messages__item">
      {!userInput ? (
        <>
          <div className="chat-item-left">
            <img
              src={item.avatar}
              className="chat-body-messages__item-avatar chat-item-left"
              alt={`${item.user}'s avatar`}
            />
          </div>
          {ConvertDate(item.createdAt)}
          <div className="chat-item-middle">
            <p className="chat-body-messages__item-text">{item.text}</p>
            <p className="chat-body-messages__item-date">{item.createdAt}</p>
          </div>
          <div className="chat-item-right">
            {!isLike ? (
              <>
                <button onClick={handleLikeActive}>
                  <LikeIcon />
                </button>
              </>
            ) : (
              <>
                <button onClick={handleLike}>
                  <ActiveLikeIcon />
                </button>
              </>
            )}
          </div>
        </>
      ) : (
        <>
          <div className="chat-item-user">
            <p>{item}</p>
          </div>
        </>
      )}
    </li>
  );
};
