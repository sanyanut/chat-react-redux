//import { FETCH_DATA } from "../actions/actionTypes";
import {
  REQUEST_DATA,
  RECEIVE_DATA,
  ERROR_DATA,
  ADD_MESSAGE,
  EDIT_MESSAGE,
  LIKE_MESSAGE,
} from "../actions/actionTypes";

const initialState = {
  isFetching: false,
  items: [],
  error: false,
};

export const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_DATA:
      return { ...state, isFetching: true, error: false };
    case RECEIVE_DATA:
      return {
        ...state,
        isFetching: false,
        items: action.data,
      };
    case ERROR_DATA:
      return { ...state, isFetching: false, error: true };
    case ADD_MESSAGE:
      return { ...state, items: [...state.items, action.payload] };
    case LIKE_MESSAGE:
      return { ...state };
    case EDIT_MESSAGE:
      return { ...state };
    default:
      return state;
  }
};
