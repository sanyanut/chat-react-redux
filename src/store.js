import { createStore, applyMiddleware } from "redux";

import thunk from "redux-thunk";
import { logger } from "redux-logger";
import { combineReducers } from "redux";

import { rootReducer } from "./reducers/rootReducer";
import { userReducer } from "./reducers/userReducer";

//const reducer = combineReducers({ rootReducer, userReducer });

const store = createStore(rootReducer, applyMiddleware(thunk, logger));

export default store;
